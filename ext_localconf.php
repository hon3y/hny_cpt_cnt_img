<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntImg',
            'Hivecptcntimgshowfalimage',
            [
                'FalImage' => 'showFalImage'
            ],
            // non-cacheable actions
            [
                'FalImage' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntimgshowfalimage {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_img') . 'Resources/Public/Icons/user_plugin_hivecptcntimgshowfalimage.svg
                        title = LLL:EXT:hive_cpt_cnt_img/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_img_domain_model_hivecptcntimgshowfalimage
                        description = LLL:EXT:hive_cpt_cnt_img/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_img_domain_model_hivecptcntimgshowfalimage.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntimg_hivecptcntimgshowfalimage
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntimgshowfalimage >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hive_cpt_cnt_img {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = FAL Image
                            description = Pure FAL Image without Flux
                            tt_content_defValues {
                                 CType = list
	     	                     list_type = hivecptcntimg_hivecptcntimgshowfalimage
                            }
                        }
                        show := addToList(hive_cpt_cnt_img)
                    }

                }
            }'
        );

    }, $_EXTKEY
);