<?php
namespace HIVE\HiveCptCntImg\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class FalImageTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntImg\Domain\Model\FalImage
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntImg\Domain\Model\FalImage();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getFalImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getFalImage()
        );
    }

    /**
     * @test
     */
    public function setFalImageForFileReferenceSetsFalImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setFalImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'falImage',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFocusXReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getFocusX()
        );
    }

    /**
     * @test
     */
    public function setFocusXForFloatSetsFocusX()
    {
        $this->subject->setFocusX(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'focusX',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getFocusYReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getFocusY()
        );
    }

    /**
     * @test
     */
    public function setFocusYForFloatSetsFocusY()
    {
        $this->subject->setFocusY(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'focusY',
            $this->subject,
            '',
            0.000000001
        );
    }
}
