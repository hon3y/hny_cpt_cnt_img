<?php
namespace HIVE\HiveCptCntImg\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ClientUtility;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;

/***
 *
 * This file is part of the "hive_cpt_cnt_img" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * FalImageController
 */
class FalImageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * falImageRepository
     *
     * @var \HIVE\HiveCptCntImg\Domain\Repository\FalImageRepository
     * @inject
     */
    protected $falImageRepository = NULL;

    /**
     * action showFalImage
     *
     * @return void
     */
    public function showFalImageAction()
    {
        $aSettings = $this->settings;
        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('bDebug', $aSettings['bDebug']);
    }
}
